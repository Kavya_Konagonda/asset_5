public class addOrRemoveRowController {
    Account account = new Account();
    public list<Account> listAccount{ get; set; }
    
    public addOrRemoveRowController() {
        listAccount=new list<Account>();
        listAccount.add(account);
    }
    
    Public void addAccount() {
        Account acc = new Account();
        listAccount.add(acc);
    }
    
    public void removeAccount(){
        Integer indexVal = Integer.valueof(system.currentpagereference().getparameters().get('index'));  
        if(indexVal>1) {
        listAccount.remove(indexVal - 1);  
        }
    }    
    
    public PageReference saveAccount(){
       if(listAccount.size()>0){
            insert listAccount; 
            }
        return Page.successMessage;
    }
}